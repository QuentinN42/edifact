"""
Just testing pymongo lib

author: QuentinN42

https://www.w3schools.com/python/python_mongodb_sort.asp
"""
from os import getenv

import pymongo

# ----- START OF HTTP REQUEST -----

user = "aaa"
with open("example.edi") as f:
    data = {
        "file": f.read()
    }

# -----   END OF HTTP REQUEST -----


if getenv("DB_URL") is None:
    raise EnvironmentError("You must set DB_URL env var !")

if getenv("DB_NAME") is None:
    raise EnvironmentError("You must set DB_NAME env var !")

db = pymongo.MongoClient(getenv("DB_URL"))[getenv("DB_NAME")]


# x = db[user].insert(data)

# print(x.inserted_id)

for d in db[user].find():
    print(d["file"])

# Edifact rest

Making a simple REST API that can store and retrive some [edifact](https://en.wikipedia.org/wiki/EDIFACT) documents.

Using [flask restfull](https://flask-restful.readthedocs.io/en/latest/) for the API.

Using [mongo db](https://www.mongodb.com/) for the database.
